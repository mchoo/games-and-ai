/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
/* This class provides the A* algorithm */
public static class AStarHelper
{
	/* Validate the path nodes */
	public static bool Invalid<T>(T inNode) where T: IPathNode<T>
	{
		if(inNode == null || inNode.Invalid)
			return true;
		return false;
	}
 
	/* Calculate the distance between path nodes */
	static float Distance<T>(T start, T goal) where T: IPathNode<T>
	{
		if(Invalid(start) || Invalid(goal))
			return float.MaxValue;
		return Vector3.Distance(start.Position, goal.Position);
	}
 
	/* Base cost Estimate to move between nodes */
	static float HeuristicCostEstimate<T>(T start, T goal) where T: IPathNode<T>
	{
		return Distance(start, goal);
	}
 
	/* Find the lowest score path */
	static T LowestScore<T>(List<T> openset, Dictionary<T, float> scores) where T: IPathNode<T>
	{
		int index = 0;
		float lowScore = float.MaxValue;
 
		for(int i = 0; i < openset.Count; i++)
		{
			if(scores[openset[i]] > lowScore)
				continue;
			index = i;
			lowScore = scores[openset[i]];
		}
 
		return openset[index];
	}
 
 
	/* Execute A* algorithm */
	public static List<T> Calculate<T>(T start, T goal) where T: IPathNode<T>
	{
        /* Uncomment these lines if timing the A* calculation */
        /*System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
        timer.Start();*/
		/* */

        List<T> closedset = new List<T>();
		List<T> openset = new List<T>(); 
		openset.Add(start);
        /* Navigated nodes */
     	Dictionary<T, T> came_from = new Dictionary<T, T>();
 
		Dictionary<T, float> g_score = new Dictionary<T, float>();
        /* Cost from start along best known path. */
        g_score[start] = 0.0f; 
 
		Dictionary<T, float> h_score = new Dictionary<T, float>();
		h_score[start] = HeuristicCostEstimate(start, goal); 
 
		Dictionary<T, float> f_score = new Dictionary<T, float>();
        /* Estimated total cost from start to goal. */
        f_score[start] = h_score[start];
 
		while(openset.Count != 0)
		{
			T x = LowestScore(openset, f_score);
			if(x.Equals(goal))
			{
				List<T> result = new List<T>();
				ReconstructPath(came_from, x, ref result);

                //timer.Stop();
                //Debug.Log("Time: " + (double)timer.ElapsedTicks / (double)System.Diagnostics.Stopwatch.Frequency);

				return result;
			}
			openset.Remove(x);
			closedset.Add(x);
			foreach(T y in x.Connections)
			{
				if(AStarHelper.Invalid(y) || closedset.Contains(y))
					continue;
				float tentative_g_score = g_score[x] + Distance(x, y);
 
				bool tentative_is_better = false;
				if(!openset.Contains(y))
				{
					openset.Add(y);
					tentative_is_better = true;
				}
				else if (tentative_g_score < g_score[y])
					tentative_is_better = true;
 
				if(tentative_is_better)
				{
					came_from[y] = x;
					g_score[y] = tentative_g_score;
					h_score[y] = HeuristicCostEstimate(y, goal);
					f_score[y] = g_score[y] + h_score[y];
				}
			}
		}
 
     return null;
 
	}
 
	/* Reconstruct the path once the destination has been found */
	static void ReconstructPath<T>(Dictionary<T, T> came_from, T current_node, ref List<T> result) where T: IPathNode<T>
	{
		if(came_from.ContainsKey(current_node))
		{
			ReconstructPath(came_from, came_from[current_node], ref result);
			result.Add(current_node);
			return;
		}
		result.Add(current_node);
	}
}