/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IPathNode<T>
{
	List<T> Connections { get; }
	Vector3 Position { get; }
	bool Invalid {get;}
}