/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
 
/* This class provides the creation of the grid used for pathfinding 
 * using the DrawHelper class and calculates paths on demand using
 * AStarHelper class */
public class PathNodeTester : MonoBehaviour
{
    /* Parameters for splitting the ground into grid nodes */
	public int gridSizeX = 40;
	public int gridSizeY = 40;
	public List<PathNode> sources;
    public static float minDistanceToNode = 3f;
	
    /* Vectors representing the begin and end nodes for pathfinding */
    public Vector3 start;
	public Vector3 end;
	
    /* Debug info */
    public Color nodeColor = new Color(0.05f, 0.3f, 0.05f, 0.1f);
	public Color pulseColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);
	public Color pathColor = new Color(1.0f, 0.0f, 0.0f, 1.0f);
	
    /* Control variables */
    public bool reset;
    public bool gridCreated;
	int startIndex;
	int endIndex;
    int lastEndIndex;
	int lastStartIndex;
    bool donePath;
	
    /* The calculated path */
    public List<PathNode> solvedPath = new List<PathNode>();
 
    /* Flag to control if a path has been calculated */
 	public bool DonePath
	{
		get { return donePath; }	
	}
	
    /* Initialize values */
	public void Start(){
		start = new Vector3(0, -10, 0);
		end = new Vector3(0, -10, 0);
	}
	
    /* Create the grid for pathfinding */
	public void Awake()
	{
		if(gridCreated)
			return;
		//create path node grid 
		sources = PathNode.CreateGrid(Vector3.zero, Vector3.one * 5.0f, new int[] { gridSizeX, gridSizeY}, 0.0f);
		gridCreated = true;
 
	}
    
    /* Draw debug squares on nodes */
	public void PulsePoint(int index)
	{
		if(AStarHelper.Invalid(sources[index]))
			return;
		DrawHelper.DrawCube(sources[index].Position, Vector3.one * 2.0f, pulseColor);
	}
 
    /* Draw debug line representing the path */
	public void Draw(int startPoint, int endPoint, Color inColor)
	{
		Debug.DrawLine(sources[startPoint].Position, sources[endPoint].Position, inColor);
	}

    /* Obtain a new path using A* */
    public List<PathNode> GetNewPath(Vector3 start, Vector3 end)
    {
        int startIndex = Closest(sources, start);
        int endIndex = Closest(sources, end);

        for (int i = 0; i < sources.Count; i++)
        {
            if (AStarHelper.Invalid(sources[i]))
                continue;
        }

        return AStarHelper.Calculate(sources[startIndex], sources[endIndex]);
    }

    /* Calculate the closest node to a position in the map */
    public static int Closest(List<PathNode> inNodes, Vector3 toPoint, bool learning = false)
    {
        int closestIndex = 0;
        float minDist = float.MaxValue;
        int k = 0;

        /*********QUICK FIX**********/
        /* Calculate the closest column of nodes and 
         * start to look for distances from there 
         * assuming we have 32 columns in the grid */
        
        if (learning)
        {
            float xPos = 0, pos;
            pos = (toPoint.x > 0 ? toPoint.x + 84 : System.Math.Abs(toPoint.x));
            float size = 168.0f / 32f;
            for (k = 0; k < 32; k++)
            {
                xPos += size;
                if (xPos > pos)
                    break;
            }
            if (k <= 0) k = 0; else k = (k - 1) * 32;
            /*****************************/
        }

        for (int i = k; i < inNodes.Count; i++)
        {
            if (AStarHelper.Invalid(inNodes[i]))
                continue;
            float thisDist = Vector3.Distance(toPoint, inNodes[i].Position);
            if (thisDist > minDist)
                continue;

            minDist = thisDist;
            closestIndex = i;
            if (minDist < minDistanceToNode)
                break;
        }

        return closestIndex;
    }
 
    /* If a path is required, perform calculations */
	public void Update()
	{
		if(reset)
		{
			donePath = false;
			solvedPath.Clear();
			reset = false;
		}
 
        if(start == new Vector3(0, -10, 0) || end == new Vector3(0, -10, 0))
		{
			return;
		}
 
		startIndex = Closest(sources, start);
		endIndex = Closest(sources, end);
 
        /* Don't calculate a path if the start or end nodes haven't changed */
		if(startIndex != lastStartIndex || endIndex != lastEndIndex)
		{
			reset = true;
			lastStartIndex = startIndex;
			lastEndIndex = endIndex;
			return;
		}
 
        /* Check for node validity */
		for(int i = 0; i < sources.Count; i++)
		{
			if(AStarHelper.Invalid(sources[i]))
				continue;
			sources[i].nodeColor = nodeColor;
		}
 
        /* Highlight nodes */
		PulsePoint(lastStartIndex);
		PulsePoint(lastEndIndex);
 
        /* Calculate the path */ 
		if(!donePath)
		{
			solvedPath = AStarHelper.Calculate(sources[lastStartIndex], sources[lastEndIndex]);
			donePath = true;
		}
 
		// Invalid path
		if(solvedPath == null || solvedPath.Count < 1)
		{
			Debug.LogWarning("Invalid path!");
			reset = true;
			enabled = false;
			return;
		}
 
 		//Draw path	
		for(int i = 0; i < solvedPath.Count - 1; i++)
		{
			if(AStarHelper.Invalid(solvedPath[i]) || AStarHelper.Invalid(solvedPath[i + 1]))
			{
				reset = true;
				return;
			}
			Debug.DrawLine(solvedPath[i].Position, solvedPath[i + 1].Position, Color.red); 
		} 
	}
}