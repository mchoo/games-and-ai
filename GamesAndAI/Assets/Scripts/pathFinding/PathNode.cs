/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* This class provides the representation of a path node in the map and its connections */
public class PathNode : MonoBehaviour, IPathNode<PathNode>
{
	public List<PathNode> connections;
 
	public static int pnIndex;

	public Color nodeColor = new Color(0.05f, 0.3f, 0.05f, 0.1f);
 
	public bool Invalid
	{
		get { return (this == null); }
	}
 
	public List<PathNode> Connections
	{
		get { return connections; }
	}
 
	public Vector3 Position
	{
		get
		{
 
			return transform.position;
		}
	}
 
	public void Update()
	{
		DrawHelper.DrawCube(transform.position, Vector3.one, nodeColor );
		if(connections == null)
			return;
		for(int i = 0; i < connections.Count; i++)
		{
			if(connections[i] == null)
				continue;
			Debug.DrawLine(transform.position, connections[i].Position, nodeColor);
		}
	}
 
	public void Awake()
	{
		if(connections == null)
			connections = new List<PathNode>();
	}
	
    /* Spawn the nodes in the map */
	public static PathNode Spawn(Vector3 inPosition)
	{
		PathNode newNode = null;
		RaycastHit hitInfo;
		GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
		obj.collider.isTrigger = true;
		obj.name = "pn_" + pnIndex;
		obj.transform.position = inPosition;
		obj.tag = "Path Node";
        
		//Don't render path nodes
		obj.renderer.enabled= false;
		
		//Check for obstacles
		//only destroy the node when there is obstacle on top of the node
		if(Physics.Raycast(obj.transform.position, Vector3.up, out hitInfo,2.0f)){
			if(hitInfo.collider.tag == "obstacle"){
				Destroy(obj);
			}
			else{
				//Debug.Log(hitInfo.collider.tag);
				pnIndex++;
				newNode = obj.AddComponent<PathNode>();
			}
		}
		else {
			pnIndex++;
			newNode = obj.AddComponent<PathNode>();
		}
		
		return newNode;
	}
 
    /* Create the grid of nodes for pathfinding */
	public static List<PathNode> CreateGrid(Vector3 center, Vector3 spacing, int[] dim, float randomSpace)
	{
		//spawn on top of level
		GameObject objectLoc = GameObject.Find("Plane");
		
		GameObject groupObject = new GameObject("grid");
		Random.seed = 1337;
		int xCount = dim[0];
		int yCount = dim[1];
		float xWidth = spacing.x * xCount;
		float yWidth = spacing.z * yCount;
 
		float xStart = center.x - (xWidth / 2.0f) + (spacing.x / 2.0f);
		float yStart = center.z - (yWidth / 2.0f) + (spacing.z / 2.0f);
 
		List<PathNode> result = new List<PathNode>();
 
		for(int x = 0; x < xCount; x++)
		{
			float xPos = (x * spacing.x) + xStart;
 
			for(int y = 0; y < yCount; y++)
			{
				if(randomSpace > 0.0f)
				{
					if(Random.value <= randomSpace)
					{
						result.Add(null);
						continue;
 
					}
				}
				float yPos = (y * spacing.z) + yStart;
				
				PathNode newNode = Spawn(new Vector3(objectLoc.transform.position.x + xPos, objectLoc.transform.position.y + 0.5f, objectLoc.transform.position.z + yPos));

				if (newNode != null)
				{
					result.Add(newNode);
					newNode.transform.parent = groupObject.transform;
				}
				else
				{
					result.Add(null);
					continue;
				}
			}
		}
 
		for(int x = 0; x < xCount; x++)
		{
 
 
			for(int y = 0; y < yCount; y++)
			{
				int thisIndex = (x * yCount) + y;
				List<int> connectedIndicies = new List<int>();
				PathNode thisNode = result[thisIndex];
				if(AStarHelper.Invalid(thisNode)) continue;
				
				if(x != 0)
					connectedIndicies.Add(((x - 1) * yCount) + y);
				if(x != xCount - 1)
					connectedIndicies.Add(((x + 1) * yCount) + y);
				if(y != 0)
					connectedIndicies.Add((x * yCount) + (y - 1));
				if(y != yCount - 1)
					connectedIndicies.Add((x * yCount) + (y + 1));
 
				if(x != 0 && y != 0)
					connectedIndicies.Add(((x - 1) * yCount) + (y - 1));
 
				if(x != xCount - 1 && y != yCount - 1)
					connectedIndicies.Add(((x + 1) * yCount) + (y + 1));
 
				if(x != 0 && y != yCount - 1)
					connectedIndicies.Add(((x - 1) * yCount) + (y + 1));
 
				if(x != xCount - 1 && y != 0)
					connectedIndicies.Add(((x + 1) * yCount) + (y - 1));
 
				for(int i = 0; i < connectedIndicies.Count; i++)
				{
					PathNode thisConnection = result[connectedIndicies[i]];
					if(AStarHelper.Invalid(thisConnection))
						continue;
					thisNode.Connections.Add(thisConnection);
				}
 
			}
		}
 
		return result;
 
	}
}