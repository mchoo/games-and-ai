/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* This class holds logic to calculate
*  every simple steering behaviour involved in game */
public class movementCS : MonoBehaviour {
	//varirable 
	public float Speed = 1.0f;
	public float Accel = 20.0f;
	private float rotationSpeed = 5.0f;
	public float maxSpeed = 5.0f;
	public float maxForce = 20.0f;
	//for arrival
	private float slowingDist = 5.0f;
	private int index = 0;
	private bool finished = false, m_bCollided = false;
	public List<float> sensorDistances = new List<float>(), feelers = new List<float>();
	public float collisionDistanceThreshold = 2f, RotationTolerance = 0.0f;
	//boolean for neural networking running
	public bool NNcheck = true; 
	

	//private bool initWander = false;
	//Vector3 newTarget = new Vector3();
	//Vector3 oldTarget = new Vector3();
	
	void Start()
	{
		//if(Application.loadedLevelName == "Neuraltest")
		//	NNcheck = true;
	}
	
	//getter
	public bool Finished{
		get{
			return finished;
		}
	}
	
	//setter for index value
	public void setIndex(int val){
		index = val;
	}
	
	//setter for finish
	public void setFinished(bool boolean){
		finished = boolean;
	} 
	
	//truncate function that is use in steering behaviour later
	public Vector3 truncate(Vector3 vec, float max){
	    float i;
		i = max / vec.magnitude;
		i = i < 1.0f ? i : 1.0f;
		vec /= i;
		
		return vec;
	}
	
	// Use this for initialization
	// Vector3 targetVel
	// Default  movement code for this project
	public void Move(Vector3 targetVel) {
		if(targetVel.sqrMagnitude > 1)
			targetVel.Normalize();
		
		targetVel *= Speed;
	   
		var acceleration = (targetVel - rigidbody.velocity) * Accel;
		
		if(acceleration.sqrMagnitude > Accel * Accel)
			acceleration = acceleration.normalized * Accel;
		
		rigidbody.velocity += acceleration * Time.deltaTime;
	}
	
	//Seek steering behaviour
	public void Seek(Vector3 _targetPoint){
		Vector3 direction = new Vector3();
		direction = _targetPoint - transform.position;
		direction.y = 0;
		
		//if(direction.sqrMagnitude > 1)
		direction.Normalize();
		
		//double the speed if police persuing 
		if(gameObject.transform.name == "Police" && gameObject.GetComponent<Police>().fsm.GetCurrentState() == Chase.Instance)
			maxSpeed *= 2;
		
		Vector3 desired_velo = direction * maxSpeed;
		Vector3 steer = desired_velo - rigidbody.velocity;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		Vector3 finalSteerVec = truncate(steer, maxForce);
        rigidbody.AddForce(finalSteerVec);
		
		//change the speed back to normal if police not persuing 
		if(gameObject.transform.name == "Police" && gameObject.GetComponent<Police>().fsm.GetCurrentState() == Chase.Instance)
			maxSpeed /= 2;
	}
	
	public void Arrival(Vector3 _targetPoint){
		//Vector3 desired_velo = new Vector3();
		Vector3 direction = new Vector3();
		
		direction = _targetPoint - transform.position;
		direction.y = 0;
		
		//find distance between two point
		//float dist =  Vector3.Distance (_targetPoint, transform.position);
		float dist = direction.magnitude;
		direction.Normalize();
		
		//if its futher than the slowing distance
		if(dist > slowingDist){
			direction *= maxSpeed;
		}
		else{
			float remainVel = maxSpeed * dist/slowingDist;
			//Debug.Log ("remain : "  + remainVel);
			direction *= remainVel;
		}
		//Vector3 force = direction - rigidbody.velocity;

		Vector3 steer = direction - rigidbody.velocity;
		Vector3 finalSteerVec = truncate(steer, maxForce);
		//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		//Debug.Log(finalSteerVec);
		//apply force
		//rigidbody.velocity += force;
		rigidbody.AddForce(finalSteerVec);
		//rigidbody.AddForce(force);
		
		//Avoid_Obstacles(direction, finalSteerVec);
	}
	
	//overload arrival function for the mouse control character with pathfinding nodes
	public void followPath(List<PathNode> path, bool isFollower = false){
		if (isFollower)
            Avoid_Obstacles(6.5f, 6.5f, 30, 20, true);
        else
            Avoid_Obstacles(6.5f, 5, 6, 5);
		
		//finished following path
        if (path == null)
            return;
		
		//if path is finished
		if(index > path.Count - 1){
			rigidbody.velocity *= 0.75f;
			finished = true;
			return;
		}
		//get the new waypoint from path node list
		Vector3 waypoint = new Vector3(path[index].transform.position.x, transform.position.y, path[index].transform.position.z);
		
		float distance = Vector3.Distance(waypoint, transform.position);
		//Debug.Log("Distance : " + distance);
		
		if(distance < 4.0f){
			index++;
			return;
		}
		//Arrival(waypoint);
        Seek(waypoint);
	}

	//persue
	//1. Take both vehicles, find the distance between them.
	//2. Take the distance and divide by the target’s maxSpeed.
	//3. Take the target’s velocity, multiply it by T and add it to the target’s position.
	//4. Seek that new position
	//5. Update the vehicle
	public void Persue(GameObject target){
		//use obstacle avoidance when chasing
		Avoid_Obstacles(6.5f, 5, 6, 5);
		
		float distance = Vector3.Distance(transform.position, target.transform.position);
		float Number = distance/ maxSpeed;
		
		Vector3 targetPos = target.transform.position + (target.rigidbody.velocity * Number);
		Seek(targetPos);
	}
	
	//flee
	//implemented but never used in the project
	public void Flee(Vector3 _targetPoint){
		Vector3 desired_velo = _targetPoint - transform.position;
		desired_velo.y = 0;
		
		desired_velo.Normalize();
		
		desired_velo *= 2.0f;
		
		Vector3 steerForce = desired_velo - rigidbody.velocity;
		
		//rigidbody.AddForce(steerForce * -1.0f);
		rigidbody.velocity += -steerForce/3.0f * Time.deltaTime;
	}
	
	//avoid Obstacles
	public void Avoid_Obstacles(float rayFront, float raySide,
        int forwardReflect, int sideReflect, bool avoidPolice = false)
	{
		m_bCollided = true;
		bool bHit = false;
		float distance = 1.0f;
		//cast a front ray
		RaycastHit hitInfo;
		List<Vector3> raycastPoint = new List<Vector3>();
		
        //clear the sensors
		if(NNcheck)
		{
	        sensorDistances.Clear();
	        feelers.Clear();
		}
		
		raycastPoint.Add((transform.position + transform.forward * rayFront));
		raycastPoint.Add((transform.position + (-transform.right + transform.forward) * raySide));
		raycastPoint.Add((transform.position + (transform.right + transform.forward) * raySide));
		
		if(Physics.Raycast(transform.position, transform.forward, out hitInfo, rayFront))
		{
			
			if(hitInfo.collider.tag == "obstacle" ||
                (avoidPolice == true && hitInfo.transform.name.Equals("Police")))
            {
				bHit = true;
				distance = hitInfo.distance;
				if(!NNcheck)
				{
					Vector3 incoming = hitInfo.point - transform.position;
					Vector3 reflection = Vector3.Reflect(incoming, hitInfo.normal);
					Debug.DrawRay(hitInfo.point, hitInfo.normal * 10, Color.yellow);
					Debug.DrawRay(hitInfo.point, reflection * 2, Color.red);
					rigidbody.AddForce(reflection * forwardReflect);
				}
			}
            else
                m_bCollided = false;
		}//cast a side ray
		
		if(bHit == true && distance < collisionDistanceThreshold && NNcheck)
		{
            sensorDistances.Add(distance);
			//m_bCollided = true;
		}else if(NNcheck){
			sensorDistances.Add(-1);
		}

        bHit = false;

		if(Physics.Raycast(transform.position, (-transform.right + transform.forward), out hitInfo, raySide))
		{
			
            if (hitInfo.collider.tag == "obstacle" ||
                (avoidPolice == true && hitInfo.transform.name.Equals("Police")))
            {
				bHit = true;
				distance = hitInfo.distance;
				if(!NNcheck)
				{
					Vector3 incoming = hitInfo.point - transform.position;
					Vector3 reflection = Vector3.Reflect(incoming, hitInfo.normal);
					Debug.DrawRay(hitInfo.point, hitInfo.normal * 10, Color.yellow);
					Debug.DrawRay(hitInfo.point, reflection * 2, Color.blue);
					rigidbody.AddForce(reflection * sideReflect);
				}
			}			
				//Vector3 incoming = hitInfo.point - transform.position;
				//Vector3 reflection = Vector3.Reflect(incoming, hitInfo.normal);
				//Debug.DrawRay(hitInfo.point, hitInfo.normal * 10, Color.yellow);
				//Debug.DrawRay(hitInfo.point, reflection * 2, Color.blue);
				//rigidbody.AddForce(reflection * sideReflect);
            else
                m_bCollided = false;
		}
		
		if(bHit == true && distance < collisionDistanceThreshold && NNcheck)
		{
            sensorDistances.Add(distance);
			//m_bCollided = true;
		}else if(NNcheck){
			sensorDistances.Add(-1);
		}

        bHit = false;

		if(Physics.Raycast(transform.position, (transform.right + transform.forward), out hitInfo, raySide))
		{
			
            if (hitInfo.collider.tag == "obstacle" ||
                (avoidPolice == true && hitInfo.transform.name.Equals("Police")))
            {
				bHit = true;
				distance = hitInfo.distance;
				if(!NNcheck)
				{
					Vector3 incoming = hitInfo.point - transform.position;
					Vector3 reflection = Vector3.Reflect(incoming, hitInfo.normal);
					Debug.DrawRay(hitInfo.point, hitInfo.normal * 10, Color.yellow);
					Debug.DrawRay(hitInfo.point, reflection * 2, Color.blue);
					rigidbody.AddForce(reflection * sideReflect);
				}
			}	
            else
                m_bCollided = false;
		}
		
		if(bHit == true && distance < collisionDistanceThreshold && NNcheck)
		{
            sensorDistances.Add(distance);
			//m_bCollided = true;
		}else if(NNcheck){
			sensorDistances.Add(-1);
		}

        bHit = false;

		/* if rotation is less than threshole, reward genome 
		if(Mathf.Abs(transform.rotation.y) < 5 && gameObject.name == "Police")
		{
            GetComponent<Police>().m_dFitness += 1;
		}
		*/
		
		if(!NNcheck)
			return;
		
		//Debug.Log("Check!;");
		
		/* check how many times the police has visited this feelers cell */
		if(NNcheck)
		{
	        if (!GetComponent<Mapper>().initialized)
	            GetComponent<Mapper>().Init();
			List<SCell> allCells = GetComponent<Mapper>().Cells;
			SCell needed = allCells[0];
	        var tester = GameObject.Find("NodeGenerator").GetComponent<PathNodeTester>();
			bool isStuck = false;
			foreach(Vector3 rayCastPt in raycastPoint)
			{
		
				int index = PathNodeTester.Closest(tester.sources, rayCastPt, true);
			
				foreach(SCell current in allCells)
				{
					if(current.Coords == tester.sources[index].transform.position)
					{
						needed = current;
						break;
					}
				}
			
				if(isStuck)
				{
					foreach(SCell current in allCells)
					{
						if(current.Coords == tester.sources[index].transform.position)
						{
							needed = current;
							break;
						}
					}
					
					if(isStuck)
					{
						feelers.Add(-1);
						continue;
					}
					
					if(needed.Coords == GetComponent<Mapper>().CurrentNode.Coords)
					{
						isStuck = true;
						feelers.Add(-1);
						continue;
					}
					int howOften = needed.TicksLingered;
					
					if(howOften == 0)
					{
						feelers.Add(-1);
					}else if( howOften < 10)
					{
						feelers.Add(0);
					}else if(howOften < 20)
					{
						feelers.Add(0.2f);
					}else if(howOften < 30)
					{
						feelers.Add(0.4f);
					}else if(howOften < 50)
					{
						feelers.Add(0.6f);
					}else if(howOften < 80)
					{
						feelers.Add(0.8f);
					}else{
						feelers.Add(1);
					}
				}
			}
	
	        if (sensorDistances == null || feelers == null
	            || sensorDistances.Count != 3 || feelers.Count != 3) return;
	
	        List<double> inputs = new List<double>();
	        inputs.Add(sensorDistances[0]);
	        inputs.Add(sensorDistances[1]);
	        inputs.Add(sensorDistances[2]);
	        inputs.Add(feelers[0]);
	        inputs.Add(feelers[1]);
	        inputs.Add(feelers[2]);
	        
	        transform.GetComponent<Police>().testNN(inputs);
		}
	}
}
