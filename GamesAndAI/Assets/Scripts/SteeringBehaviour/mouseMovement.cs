/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(movementCS))]
public class mouseMovement : MonoBehaviour {
	//variables
	private Vector3 _targetPoint;
	private movementCS _movement;
	public GameObject mainCamera;
	private PathNodeTester _end;
	List<PathNode> path;
	public bool pathing;
	public int max_zoom = 70;
	public int min_zoom = 20;
	
	// Use this for initialization
	void Start () {
		pathing = true;
		//find the main camera object in the game scene 
	    mainCamera = (GameObject) GameObject.FindWithTag("MainCamera");
		//get the movement script 
		_movement = GetComponent<movementCS>();
		//leave it out first
		_targetPoint = transform.position;
		
		//get the component NodeGenerator 
		if(pathing)
			_end = GameObject.Find("NodeGenerator").GetComponent<PathNodeTester>();
	}
	
	// Update is called once per frame
	void Update () {
		//debug draw line of sight
		Debug.DrawRay(transform.position, transform.forward * 8, Color.blue);
		Debug.DrawRay(transform.position, (-transform.right + transform.forward) * 3, Color.blue);
		Debug.DrawRay(transform.position, (transform.right + transform.forward) * 3, Color.blue);
		
		//always move camera to the character
		mainCamera.transform.position = new Vector3(rigidbody.position.x, mainCamera.transform.position.y, rigidbody.position.z);
		
		//handle mouse button controll
		//right click to move , like normal RTS games
		//scroll to zoom in and zoom out of the level
		//if((Input.GetAxis("Mouse ScrollWheel") < 0) && (Camera.main.orthographicSize <= 60)){//scroll down
		if((Input.GetAxis("Mouse ScrollWheel") < 0) && (Camera.main.orthographicSize <= max_zoom)){
			Camera.main.orthographicSize+=5; 
		}
		if((Input.GetAxis("Mouse ScrollWheel") > 0) && (Camera.main.orthographicSize >= min_zoom)){//scroll up
			Camera.main.orthographicSize-=5; 
		}
		
		if(Input.GetMouseButton(1)){
			RaycastHit hitInfo;
			var ray = Camera.mainCamera.ScreenPointToRay(Input.mousePosition);
			
			//ray cast when user click, and get the point of where the user clicked
			if(Physics.Raycast(ray, out hitInfo, Mathf.Infinity)){
				_targetPoint = hitInfo.point;
				_targetPoint.y = 0;
			}
			//if enabled A* path finding and right clicked, assign a start pos and end pos
			// to compute a paths
			if(pathing){
				_end.start = rigidbody.position;
				_end.end = _targetPoint;
			}

		}
		//when right click release, reset everthinh
		if(Input.GetMouseButtonUp(1)){
			_movement.setIndex(0);
			_movement.setFinished(false);
		}
		
		//steering bahaviours call
		if(pathing){
			//if path is generated, follow the path that created
			if(_end.DonePath){	
				path = _end.solvedPath;
				_movement.followPath(path);
			}
		}
		else
			_movement.Arrival(_targetPoint);//if not using pathfinding then use Arrival behaviour
	}
}
