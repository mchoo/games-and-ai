using UnityEngine;
using System.Collections;

public class NewWandering : MonoBehaviour {

private Vector3 destination  = Vector3.zero;
	private float distance = 4.0f;

	private movementCS _movement;
	
	
	void Start()
	{
		Random.seed = System.Environment.TickCount;
		_movement = GetComponent<movementCS>();
		InvokeRepeating("ChooseRandomDestination", 0.0f, 2.0f);
	}
	
	void Update()
	{
		rigidbody.velocity = destination;
		//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(destination), 1 * Time.deltaTime);
		
		transform.rotation = new Quaternion(0, transform.rotation.y, 0, transform.rotation.w);
	}
 

	void ChooseRandomDestination(){
		float randNum = Random.Range(0.0f, 1.0f);
		Debug.Log (randNum);
		if(randNum >= 0.75)
		{
			destination = Vector3.forward * distance;
		}else if(randNum >= 0.5 && randNum < 0.75)
		{
			destination = Vector3.left * distance;
		}else if(randNum >= 0.25 && randNum < 0.5)
		{
			destination = Vector3.right * distance;
		}else{
			destination = Vector3.forward;
		}
	}
}
