/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(movementCS))]
public class AIManager : MonoBehaviour {
	private movementCS _movement;
	public GameObject pTarget;
	
	// Use this for initialization
	void Start () {
	    pTarget = (GameObject) GameObject.Find("Leader");
		//get the movement script 
		_movement = GetComponent<movementCS>();
	}
	
	// Update is called once per frame
	void Update () {
		//steering bahaviours call
		//var targetVelo = _targetPoint - transform.position;
		//_movement.Seek(_targetPoint);
		//_movement.Move(targetVelo);
		_movement.Persue(pTarget);
		//_movement.Arrive(_targetPoint);
		//_movement.Flee(pTarget.transform.position);
		//_movement.Flee(pTarget.transform.position);
	}
}
