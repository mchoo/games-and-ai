/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class holds the logic to apply the wandering behaviour to a game character */
public class Wandering : MonoBehaviour {
	private Vector3 startDirection;
	private Vector3 futureDirection;
	public float strength;
	public float rate;
	public float speed;
	private bool obstacle;
	public float rotationSpeed = 5.0f;
	private movementCS actions;
	RaycastHit hitInfo;
	
	// Use this for initialization
	void Start () {
		Random.seed = System.Environment.TickCount;
		Vector2 circle = Random.insideUnitCircle;
		Vector2 start = new Vector2(transform.position.x, transform.position.z) + circle * 10;
		float absY = Mathf.Abs(start.y);//get the absolute value of y
		
		startDirection = new Vector3(start.x, 10, absY);
		
		Ray ray = new Ray(transform.position, startDirection);
		obstacle = Physics.Raycast(ray, 2);
		//Debug.Log (obstacle);
		if(!obstacle)
		{
			transform.position = Vector3.Lerp(transform.position, startDirection, speed);
		}
		
		actions = GetComponent<movementCS>();
		InvokeRepeating("calcOffset", 0.0f, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 check = startDirection;
		check.y = 0;
		Debug.DrawRay(transform.position, check, Color.red);
		//debug draw line of sight
		Debug.DrawRay(transform.position, transform.forward * 10, Color.green);
		Debug.DrawRay(transform.position, (-transform.right + transform.forward) * 5, Color.green);
		Debug.DrawRay(transform.position, (transform.right + transform.forward) * 5, Color.green);
		//futureDirection = currentDirection + calcOffset();
		
		actions.Avoid_Obstacles(6.5f, 6.5f, 10, 7);
		movement(startDirection);
		//Vector3.MoveTow = calcOffset();
		//transform.position = Vector3.Lerp(transform.position, offset, speed * Time.deltaTime);
		//transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(startDirection), rotationSpeed * Time.deltaTime);
		
		transform.rotation = new Quaternion(0, transform.rotation.y, 0, transform.rotation.w);
		//Avoid_Obstacles();
	}
	
	void calcOffset()
	{
		//Vector2 newPos = new Vector2(transform.position.x, transform.position.z) + Random.insideUnitCircle + new Vector2(0, Mathf.Sqrt(2));
		//Debug.Log (newPos);
		//Random.seed = System.Environment.TickCount;
		Vector3 newPosVec3 = Vector3.zero;
		if(GetComponent<movementCS>().NNcheck)
			newPosVec3 = transform.forward; 
		else
			newPosVec3 = new Vector3(startDirection.x + Random.Range(-0.1f, 0.1f) + Random.Range(-10, 10), transform.position.y, startDirection.z + Random.Range(-0.1f,0.1f) + Random.Range(-10,10));
		//newPosVec3.z = Mathf.Abs(newPosVec3.z);  -- do we still need this? It doesn't work well with the below code
		if(Physics.Raycast(transform.position, transform.forward, out hitInfo, 10.0f))
		{
			if((hitInfo.transform.name == "Wall") || 
                (hitInfo.transform.name == "Police") ||
                (hitInfo.transform.name == "Door"))
			{
				newPosVec3.z = -newPosVec3.z;
				newPosVec3.x = -newPosVec3.x;
			}
		}
		//Debug.Log(startDirection);
		//transform.position = Vector3.Lerp(transform.position, newPosVec3, speed * Time.deltaTime);
		startDirection = newPosVec3;
		
		//Debug.DrawLine(transform.position, transform.position + Vector3.forward + Vector3.right, Color.red);
	}
	
	//Works like seek but without computing a direction vector
	void movement(Vector3 direction){
		direction.Normalize();
		
		Vector3 desired_velo = direction * actions.maxSpeed;
		Vector3 steer = desired_velo - rigidbody.velocity;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
		Vector3 finalSteerVec = actions.truncate(steer, actions.maxForce);
        rigidbody.AddForce(finalSteerVec);	
	}
}
