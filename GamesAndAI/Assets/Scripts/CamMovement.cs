using UnityEngine;
using System.Collections;

public class CamMovement : MonoBehaviour {
	public int max_zoom = 100;
	public int min_zoom = 20;
	
	// Update is called once per frame
	void Update () {
		if((Input.GetAxis("Mouse ScrollWheel") < 0) && (Camera.main.orthographicSize <= max_zoom)){
			Camera.main.orthographicSize+=5; 
		}
		if((Input.GetAxis("Mouse ScrollWheel") > 0) && (Camera.main.orthographicSize >= min_zoom)){//scroll up
			Camera.main.orthographicSize-=5; 
		}
		
		
		if(Input.GetKey(KeyCode.A))
			transform.Translate(new Vector3(-15* Time.deltaTime, 0, 0));			
		if(Input.GetKey(KeyCode.D))
			transform.Translate(new Vector3(15* Time.deltaTime, 0, 0));
	}
}
