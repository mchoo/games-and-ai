using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {
	private bool clicked;
	private Rect windowRect = new Rect (0, 0, 200, 100);
	
	void start() {
		clicked = false;	
	}
	
	void OnGUI () {
		// Make a label that uses the "box" GUIStyle.
		if(!clicked){
			windowRect = GUI.Window (0, windowRect, WindowFunction, "Tutorial");
		}
	}
	
	void WindowFunction (int windowID) {
		// Draw any Controls inside the window here
		GUI.Label (new Rect (20,15,180,100), "Right Click -> Move\n");
		GUI.Label (new Rect (20,30,180,100), "MouseScroll -> Zoom in/out\n");
		GUI.Label (new Rect (20,45,180,100), "H -> Hide prisoner\n");
		//button
		if (GUI.Button (new Rect (50,70,100,20), "Ok"))
            clicked = true;
	}
}
