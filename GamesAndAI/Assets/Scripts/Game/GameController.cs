/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {
	
	public string[] levelName;
	public float wordOffset = 300;
	private GameObject leader = null;
	private GameObject[] flock = null;
	private bool flockInactive = true;
	public bool isFinished = false;
	public GUIStyle TitleStyle;
	public GUIStyle MenuStyle;
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void OnGUI () {
	
		/*menu scene */
		if(levelName[0] == Application.loadedLevelName)
		{
			flock = null;
			leader = null;
            GUI.Label(new Rect(Screen.width/2 - wordOffset, Screen.height/4, Screen.width, Screen.height), 
                "Prison Break", TitleStyle);
			if(GUI.Button(new Rect(Screen.width / 2 - 125, Screen.height / 2 + 100, 250, 50), "Start Game", MenuStyle))
			{
				isFinished = false;
				Application.LoadLevel(levelName[1]); //load the game scene
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 50, Screen.height / 2 + 150, 200, 50), "Exit", MenuStyle))
			{
				isFinished = false;
				Application.LoadLevel(levelName[1]); //load the game scene
			}
			if(GUI.Button(new Rect(Screen.width / 2 - 125, Screen.height / 2 + 200, 300, 50), "Learning", MenuStyle))
			{
				isFinished = false;
				Application.LoadLevel(2); //load the game scene
			}
		}
		
		/* assign variables once they're active */
		if(GameObject.Find("Leader") != null)
		{
			if(GameObject.Find("Leader").GetComponent<Prisoner>().fsm.GetCurrentState() != RunAway.Instance)
			{
				leader = GameObject.Find("Leader");
				flock = GameObject.FindGameObjectsWithTag("Prisoner");
			}
		}
		
		/* leader is dead */
		if(leader != null && leader.active == false)
		{
			GUI.Box (new Rect(0,0, Screen.width, Screen.height), "GAME OVER! Press 'r' to restart. Press 'esc' to quit");
			isFinished = true;
			//Debug.Log("ldead");
			if(Input.GetKeyUp(KeyCode.R))
			{
				Application.LoadLevel(levelName[1]);
				isFinished = false;
				leader = null;
				flock = null;
				return;
			}
		}
		
		flockInactive = true; /* inactive until proven active */
		
		/*check if flock is dead, if so, end game */
		if(flock != null)
		{
			foreach(GameObject go in flock)
			{
				if(go.name == "Leader")
					continue;
				if(go.active)
				{
					flockInactive = false;
					break;
				}
			}
			
			if(flockInactive)
			{
				leader.GetComponent<movementCS>().enabled = false;
				leader.GetComponent<mouseMovement>().enabled = false;
				GUI.Box (new Rect(0,0, Screen.width, Screen.height), "GAME OVER! Press 'esc' to quit(esc will not work in editor mode)");
				isFinished = true;
			}
		}
		
		/* win text if leader has won */
		if(leader != null)
		{
			if(leader.GetComponent<Prisoner>().fsm.GetCurrentState() == RunAway.Instance)
			{
				GUI.Box (new Rect(0,0, Screen.width, Screen.height), "YOU WIN!Press 'esc' to quit(esc will not work in editor mode) R to restart.");
				isFinished = true;
				//Debug.Log("won  +  bool  : " + isFinished);		
			}
			if(Input.GetKeyUp(KeyCode.R))
			{
				Application.LoadLevel(levelName[1]);
				isFinished = false;
				leader = null;
				flock = null;
				return;
			}
		}		
		
		/* quit */
		/*Escape will not work in editor mode*/
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Application.Quit();
		}
		
	}
	
	public bool IsFinished
	{
		get{
			return isFinished;
		}
	}
}
