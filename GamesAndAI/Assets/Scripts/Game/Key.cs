using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour {
    /* Key management to escape the prison */
	void OnTriggerEnter(Collider other) {
		if(other.name == "Leader"){
        	Destroy(this.gameObject);
			Destroy(GameObject.FindGameObjectWithTag("KeyDoor"));
		}
    }
}
