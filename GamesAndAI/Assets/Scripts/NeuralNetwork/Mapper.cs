/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Mapper : MonoBehaviour {

	// the 2D vector of memory cells
	private List<SCell> m_2DvecCells; 
	private int m_NumCellsY;
	private int m_NumCellsX;
	private int m_NumCellsZ;
	private int m_iTotalCells;
    public bool initialized = false;
	private List<SCell> cellsVisited;
    private PathNodeTester nodeTester=null;
	private SCell currentNode = null;
	
	// the dimensions of each cell
	private float m_fCellSize;
	
	void Start()
	{
        if (m_2DvecCells == null)
            m_2DvecCells = new List<SCell>();
		m_NumCellsY = 0;
		m_NumCellsX = 0;
		m_NumCellsZ = 0;
		m_iTotalCells = 0;
	}
	
	/* sets up all cell co-ords */
	public void Init()
	{
        if (m_2DvecCells == null)
            m_2DvecCells = new List<SCell>();
		GameObject[] pathNodes = GameObject.FindGameObjectsWithTag("Path Node");

		foreach(GameObject go in pathNodes)
		{
			SCell newCell = new SCell(go.transform.position);
			m_2DvecCells.Add(newCell);
		}
		
		cellsVisited = new List<SCell>();
		
		initialized = true;
	}
	
	void FixedUpdate()
	{
        if (!initialized)
            Init();
		
        if (nodeTester==null)
            nodeTester = GameObject.Find("NodeGenerator").GetComponent<PathNodeTester>();

		/* get current node this police is on */
        int index = PathNodeTester.Closest(nodeTester.sources, transform.position, true);

        /* Get the closest node */
     	currentNode = m_2DvecCells.Where(cell => cell.Coords == nodeTester.sources[index].transform.position).FirstOrDefault();
		
        if (currentNode == null)
        {
            currentNode = m_2DvecCells[0];
        }

		foreach(SCell current in m_2DvecCells)
		{
			if(current.TicksLingered > 0 && !cellsVisited.Contains(current))
			{
				cellsVisited.Add(current);
				++m_iTotalCells;
			}
		}

        currentNode.IncrementTicksSpent();
	}
	
	public SCell CurrentNode
	{
		get{
			return currentNode;
		}
	}
	
	public int NumCellsVisited()
	{
		return m_iTotalCells;
	}
	
	public bool BeenVisited(SCell node)
	{
		if(node.TicksLingered > 0)
			return true;
		else
			return false;
	}
	
	public List<SCell> Cells
	{
		get{
			return m_2DvecCells;
		}
	}
}