using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ccontroller : MonoBehaviour {
	public List<SGenome> m_vecThePopulation;
	public List<Police> m_vecSweepers;//name for now just for testing
	public GameObject[] polis;// same as above
	public GeneticAlgorithm m_pGA;
	public int m_NumWeightsInNN;
	public List<double> m_vecAvFitness;
	public List<double> m_vecBestFitness;
	
	public int m_iGenerations;
	public int iNumTicks = 20;
	public double dMutationRate = 0.1;
	public double dCrossoverRate = 0.7;
	public int m_iTicks;
	
	// Use this for initialization
	void Start () {
		init();
	}
	
	// Update is called once per frame
	void Update () {
		update();
	}
	
	public void init(){
		m_iTicks = 0;
		m_vecSweepers = new List<Police>();
		m_vecAvFitness = new List<double>();
		m_vecBestFitness = new List<double>();
		
        /*Get weights from xml*/
        var weights = Logger.getWeightsFromLog();
        
		//polis = GameObject.FindGameObjectsWithTag("Police");
		//x= 40 y = 72

        List<Vector3> positions = new List<Vector3>();
        positions.Add(new Vector3(0, 10, 72));
        
        //uncomment if more positions needed and put [i] instead of [0] inside the for loop
        
        /*positions.Add(new Vector3(-44, 10, 30));
        positions.Add(new Vector3(55, 10, 42));
        positions.Add(new Vector3(55, 10, -75));
        positions.Add(new Vector3(35, 10, -58));
        positions.Add(new Vector3(-17, 10, -58));
        positions.Add(new Vector3(-45, 10, -74));
        positions.Add(new Vector3(-70, 10, -76));
        positions.Add(new Vector3(-65, 10, 24));
        positions.Add(new Vector3(-45, 10, 14));*/
        int k=0;
        for (int i = 0; i < 10; i++){
			GameObject polis = Instantiate(Resources.Load("police1"), positions[0], Quaternion.Euler(new Vector3(0,k,0))) as GameObject;
            //var pol = polis[i].GetComponent<Police>();
			var pol = polis.GetComponent<Police>();
            pol.initNN();
            m_vecSweepers.Add(pol);
            k+=36;
        }

		//get the total number of weights used in the sweepers
		//NN so we can initialise the GA
        m_NumWeightsInNN = m_vecSweepers[0].nnet.GetNumberOfWeights();
		
		//calculate the neuron placement in the weight vector
		List<int> SplitPoints = new List<int>();
		SplitPoints = m_vecSweepers[0].nnet.CalculateSpiltPoints();
		
        m_pGA = new GeneticAlgorithm(m_vecSweepers.Count, dMutationRate, dCrossoverRate, m_NumWeightsInNN, SplitPoints, weights);
		
		//Get the weights from the GA and insert into the sweepers brains
		m_vecThePopulation = m_pGA.GetChromos();
        
		/*Debug.Log ("minesweeper count : " + m_vecSweepers.Count);
		Debug.Log ("population count : " + m_vecThePopulation.Count);
		Debug.Log ("pos count : " + polis.Length);*/
		

        for (int i = 0; i < polis.Length; i++)
        {
			//Debug.Log("Weight : "+ m_vecThePopulation[i].vecWeights.Count);
            m_vecSweepers[i].nnet.PutWeights(m_vecThePopulation[i].vecWeights);
        }
	}
	
	public void update(){
        //yes its the same, post increment stuff
		if (m_iTicks++ < iNumTicks){
			for (int i=0; i<m_vecSweepers.Count; ++i){
				//Debug.Log("Wrong amount of NN inputs!");
			}
		}
        else
        {
			int BestCellCoverage = 0;

            for (int swp = 0; swp < m_vecSweepers.Count; ++swp)
            {
                //add up all the penalties and calculate a fitness score
                m_vecSweepers[swp].EndOfRunCalculations();

                //update the fitness score stored in the GA with this score
                var genome = m_vecThePopulation[swp];
                genome.dFitness = m_vecSweepers[swp].m_dFitness;

                var cellsVisited = m_vecSweepers[swp].GetComponent<Mapper>().NumCellsVisited();
                if (cellsVisited > BestCellCoverage)
                    BestCellCoverage = cellsVisited;
            }
	
			//increment the generation counter
			++m_iGenerations;
			
			//reset cycles
			m_iTicks = 0;
				
			//insert the sweepers chromosones into the GA factory and
			//run the factory to create a new population
			
			m_vecThePopulation = m_pGA.Epoch(m_vecThePopulation);

			//insert the new (hopefully)improved brains back into the sweepers
    		//and reset their positions etc
			//Debug.Log(m_vecSweepers.Count);
			for (int i=0; i<m_vecSweepers.Count; i++) {
				//Debug.Log("vecPop count : " + m_vecThePopulation[i].vecWeights.Count);
				m_vecSweepers[i].nnet.PutWeights(m_vecThePopulation[i].vecWeights);
                m_vecSweepers[i].Reset();
			}

            /* Restart after N generations */
            if (m_iGenerations % 10 == 0)
            {
                /* Write the weights for the fittest */
                var w = m_pGA.GetChromos()[m_pGA.m_iFittestGenome].vecWeights;
                Dictionary<int, List<double>> dd = new Dictionary<int, List<double>>();
                dd.Add(1, w);
                Logger.writeLog(m_iGenerations, dd);

                var ps = GameObject.FindGameObjectsWithTag("Police");
                foreach (var p in ps)
                {
                    p.active = false;
                    Destroy(p);
                }

                init();
            }
		}
	}
}
