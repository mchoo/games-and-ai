/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//SGenome
public struct SGenome{
	public List<double> vecWeights;
	public double dFitness;
	
	//ctor
	public SGenome(List<double> vecWeights, double f){
		this.vecWeights = vecWeights;
		this.dFitness = f;
	}
	
	//function for sorting
	public bool returnSort(SGenome lhs, SGenome rhs){
		return(lhs.dFitness < rhs.dFitness);	
	}
};