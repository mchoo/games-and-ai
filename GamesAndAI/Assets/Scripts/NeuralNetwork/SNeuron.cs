/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//SNeuron
public struct SNeuron {
	//the number of input into the neuron
	public int m_NumInputs;
	//weights of each input
	public List<double> m_vecWeight;

	public SNeuron(int NumInputs){
		//
		Random.seed = System.Environment.TickCount;
		
		this.m_NumInputs = NumInputs + 1;
		m_vecWeight = new List<double>();
		//need additional weight for the bias 
		for(int i = 0; i< NumInputs + 1; ++i){
			//set up weight with an initial random value
			double value = (double)Random.Range(-1.0f, 1.0f);
			m_vecWeight.Add(value);
			//Debug.Log ("giving weights : " + m_vecWeight[i]);
		}
	}
};