/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//SNeuronLayer
public struct SNeuronLayer{
	// the number of nuerons in this layer
	public int m_NumNeurons;
	//the layer of neurons
	public List<SNeuron> m_vecNeurons;
	
	public SNeuronLayer(int NumNeurons, int NumInputsPerNeuron){
		m_vecNeurons = new List<SNeuron>();
		this.m_NumNeurons = NumNeurons;
		
		for(int i= 0; i<NumNeurons; ++i)
		{
			m_vecNeurons.Add(new SNeuron(NumInputsPerNeuron));
		}
	}
};
