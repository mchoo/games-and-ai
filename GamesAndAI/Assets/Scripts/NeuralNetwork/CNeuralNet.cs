/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//CneuralNet
public class CNeuralNet {
	//private variables
	public int m_NumInputs;
	public int m_NumOutputs;
	public int m_NumHiddenLayers;
	public int m_NeuronsPerHiddenLyr;
	//storage for each layer of neurons including the output layer
	private List<SNeuronLayer> m_vecLayers;
	
	//public 
	public CNeuralNet(int m_NumInputs, int m_NumOutputs,int m_NumHiddenLayers,int m_NeuronsPerHiddenLyr){
		this.m_NumInputs = m_NumInputs;
		this.m_NumOutputs = m_NumOutputs;
		this.m_NumHiddenLayers = m_NumHiddenLayers;
		this.m_NeuronsPerHiddenLyr = m_NeuronsPerHiddenLyr;
		m_vecLayers = new List<SNeuronLayer>();
		
		CreateNet();
	}
	
	//build the network from SNeurons
	public void CreateNet(){
		//Debug.Log("creating network");
		//create the layer of the network
		if(m_NumHiddenLayers > 0){
			//create first hidden layer
			m_vecLayers.Add(new SNeuronLayer(m_NeuronsPerHiddenLyr, m_NumInputs));
			
			// fill the hidden layers
			for(int i = 0; i< m_NumHiddenLayers; ++i){
				m_vecLayers.Add(new SNeuronLayer(m_NeuronsPerHiddenLyr, m_NeuronsPerHiddenLyr));
			}
			
			//create output layer
			m_vecLayers.Add(new SNeuronLayer(m_NumOutputs, m_NeuronsPerHiddenLyr));
		}
		else{ 
			//just create output layer
			m_vecLayers.Add(new SNeuronLayer(m_NumOutputs, m_NumInputs));
		}
	}
	
	//get the weights from the Neural Network
	public List<double> GetWeights() {
		//this will hold the weights
		List<double> weights = new List<double> ();
		/*Debug.Log("num of hiddenlayer : "+m_NumHiddenLayers);

		Debug.Log("m_vecLayers count : "+ m_vecLayers.Count);*/
		
		//for each layer
		for(int i = 0; i < m_NumHiddenLayers + 1; ++i){
			//for each neuron
			for(int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j){
				//for each weight
				for(int k = 0; k < m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k){
					weights.Add(m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k]);
				}
			}
		}
		return weights;
	}
	
	//	given a vector of doubles this function replaces the weights in the NN
	//  with the new values
	public void PutWeights(List<double> weights){
		int cWeight = 0;
		//Debug.Log("weights : "+ weights.Count);
		
		//for each layer
		for (int i=0; i<m_NumHiddenLayers + 1; ++i){
			//for each neuron
			for(int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j){
				//for each weight
				for(int k = 0; k < m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k){
					//Debug.Log("cWeight : " + cWeight);
					m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] = weights[cWeight++];
					//Debug.Log("check weight : " + m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] + " k : " + k );
				}
			}
		}
		return;
	}
	
	//	returns the total number of weights needed for the net
	public int GetNumberOfWeights(){
		int weights = 0;
		
		for (int i=0; i<m_NumHiddenLayers + 1; ++i){
			for (int j=0; j<m_vecLayers[i].m_NumNeurons; ++j){
				for (int k=0; k<m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k){
					weights++;
				}
			}
		}
		return weights;
	}
	
	//  this method calculates all points in the vector of weights which 
//  represent the start and end points of individual neurons
	public List<int> CalculateSpiltPoints(){
		List<int> SplitPoints = new List<int>();
		
		int WeightCounter = 0;
		
		//for each layer
		for (int i=0; i<m_NumHiddenLayers + 1; ++i){
			for (int j=0; j<m_vecLayers[i].m_NumNeurons; ++j){
				for (int k=0; k<m_vecLayers[i].m_vecNeurons[j].m_NumInputs; ++k){
						++WeightCounter;
				}
				SplitPoints.Add(WeightCounter - 1);
			}
		}
		
		return SplitPoints;
	}
	
	//	given an input vector this function calculates the output vector
	public List<double> Update(List<double> inputs){
		//store the resultant outputs from each layer
		List<double> outputs = new List<double> ();
		
		int cWeight = 0;
		
		//first check that we have the correct amount of inputs
		if(inputs.Count != m_NumInputs){
			//just return an empty vector if incorrect
			return outputs;
		}
		
		//for each layer
		for(int i = 0; i < m_NumHiddenLayers + 1; ++i){
            if (i > 0)
            {
                //inputs = outputs; //not sure !!
				inputs = new List<double>( outputs);
			}
			
			outputs.Clear();
			cWeight = 0;
			
			//for each neuron sum the inputs * corresponding weights. Throw
			//the total at the sigmoid function to get the output.
			for(int j = 0; j < m_vecLayers[i].m_NumNeurons; ++j){
				double netinput = 0.0;
				int NumInputs = m_vecLayers[i].m_vecNeurons[j].m_NumInputs;
                
				//for each weight
                for(int k = 0; k < NumInputs -1; ++k){
					//sum the weight x inputs
					netinput += m_vecLayers[i].m_vecNeurons[j].m_vecWeight[k] * inputs[cWeight++];
				}
				
				//add in the bias
				netinput += m_vecLayers[i].m_vecNeurons[j].m_vecWeight[NumInputs-1] * -1; //-1 = Bias
				
				//store the output from each layer 
				//combine activation is first filtered through the sigmoid function
				outputs.Add(Sigmoid(netinput, 1)); // dActivationResponse = 1;
				cWeight = 0;
			}
		}
		return outputs;
	}
	
	//sigmoid function
	public double Sigmoid(double netinput, double response){
		//return (1/(1+Mathf.Exp(((float)-netinput/(float)response))));	
		return ((1 / (1 + Mathf.Exp((float)-netinput / (float)response))) - 0.5f) * 2;
	}
}
