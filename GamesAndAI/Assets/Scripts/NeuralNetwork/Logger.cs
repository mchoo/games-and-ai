﻿/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

public class Logger
{
    private static XmlDocument doc;
    private const String fileName = "weightsLog.xml";

    private static void openLog()
    {
        doc = new XmlDocument();

        try
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        } catch (Exception ex) {
            //handle ex
        }
    }

    public static void writeLog(int epoch, Dictionary<int, List<double>> policeWeights)
    {
        int i = 0;
        openLog();
        
        XmlElement newEpochEntry = doc.CreateElement("epoch");

        policeWeights.ToList().ForEach(pw =>
            {
                XmlElement policeElement = doc.CreateElement("police");
                policeElement.SetAttribute("num", pw.Key.ToString());
                newEpochEntry.AppendChild(policeElement);

                i = 0;
                pw.Value.ForEach(w =>
                {
                    XmlElement weightElement = doc.CreateElement("weight");
                    weightElement.SetAttribute("number", i.ToString());
                    weightElement.InnerText = w.ToString();
                    policeElement.AppendChild(weightElement);
                    i++;
                });
            }
            );

        if (doc.HasChildNodes)
            doc.DocumentElement.InsertAfter(newEpochEntry, doc.DocumentElement.LastChild);
        else
            doc.AppendChild(newEpochEntry);

        try
        {
            doc.Save(fileName);
        }
        catch (Exception ex)
        {
            //hanldle ex
        }
    }

    public static Dictionary<int, List<double>> getWeightsFromLog() {
         Dictionary<int, List<double>> policeWeights = null;
         
         try
         {
             if (File.Exists(fileName))
             {
                 policeWeights = new Dictionary<int, List<double>>();

                 XmlDocument doc = new XmlDocument();
                 doc.Load(fileName);

                 XmlNodeList epochNodes = doc.GetElementsByTagName("epoch");

                 if (epochNodes.Count <= 0) return null;

                 XmlNodeList policeNodes = (epochNodes[0] as XmlElement).GetElementsByTagName("police");

                 foreach (XmlElement police in policeNodes)
                 {
                     List<double> weightList = new List<double>();
                     XmlNodeList weightNodes = police.GetElementsByTagName("weight");
                     foreach (XmlElement weight in weightNodes)
                     {
                         weightList.Add(double.Parse(weight.InnerText));
                     }
                     policeWeights.Add(int.Parse(police.GetAttribute("num")), weightList);
                 }
             }
         } catch (Exception ex) {
             policeWeights = null; 
         }
        return policeWeights;
    }
}