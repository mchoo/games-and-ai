/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using UnityEngine;
using System.Collections;

public class SCell
{
	Vector3 coords;
	int iTicksSpentHere;
	
	public SCell(Vector3 coords)
	{
		this.coords = coords;
		iTicksSpentHere = 0;
	}
	
	public void IncrementTicksSpent()
	{
		iTicksSpentHere++;
	}	
	
	public Vector3 Coords
	{
		get{
			return coords;
		}
	}
	
	public int TicksLingered
	{
		get{
			return iTicksSpentHere;
		}
	}
	
	public void Reset()
  	{
    	iTicksSpentHere = 0;
  	}
}
	

