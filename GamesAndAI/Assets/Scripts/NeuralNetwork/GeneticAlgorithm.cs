/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
* Code adapted from the book AI Techniques for Games Programming
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GeneticAlgorithm {//: MonoBehaviour {
	public int iNumCopiesElite = 1;
	public int iNumElite = 4;
	public int iTournamentCompetitors = 4;
	
	//population of chromosomes
	private List<SGenome> m_vecPop = new List<SGenome>();
	//this holds the positions of the split points in the genome for use
 	//in our modified crossover operator
  	private List<int> m_vecSplitPoints;
	//generation counter
	private int m_cGeneration;
	//size of population 
	public int m_iPopSize;
	//amount of weight per chromo
	public int m_iChromoLength;
	//total fitness of population
	public double m_dTotalFitness;
	//best fitness this population
	public double m_dBestFitness;
	//average fitness
	public double m_dAverageFitness;
	//worst
	public double m_dWorstFitness;
	//keep tracks of the best genome
	public int m_iFittestGenome;
	//probability og chromesones bits will mutate(0.05 to 0.3)
	public double m_dMutationRate;
	//probability of chromesones crossing over bits(0.7 is good)
	public double m_dCrossoverRate;
	public double dMaxPerturbation = 0.3;
	
	public List<SGenome> GetChromos(){
		return m_vecPop;	
	}
	
	public GeneticAlgorithm(int m_iPopSize, double m_dMutationRate, 
		double m_dCrossoverRate, int numweights, List<int> splits, Dictionary<int, List<double>> weights){
        int k;

		Random.seed = System.Environment.TickCount;
		this.m_iPopSize = m_iPopSize;
		this.m_iChromoLength = numweights;
		this.m_dMutationRate = m_dMutationRate;
		this.m_dCrossoverRate = m_dCrossoverRate;
		this.m_dTotalFitness = 0;
		this.m_cGeneration = 0;
		this.m_iFittestGenome = 0;
		this.m_dBestFitness =0;
		this.m_dWorstFitness = 99999999;
		this.m_dAverageFitness = 0;
		this.m_vecSplitPoints = splits;
		
		//initialise population with chromosomes consisting of random
		//weights and all fitnesses set to zero
		for (int i=0; i<m_iPopSize; ++i)
		{
			m_vecPop.Add(new SGenome(new List<double>(), 0));

            if (weights == null)
            {
                for (int j = 0; j < m_iChromoLength; ++j)
                {
                    double value = (double)Random.Range(-1.0f, 1.0f);
                    m_vecPop[i].vecWeights.Add(value);
                }
            }
            else if (weights.Count == m_iPopSize)
            {
                m_vecPop[i].vecWeights.AddRange(weights[i + 1]);
            }
            else
            {
                k = weights.Count > 1 ? (int)Random.Range(1, weights.Count) : 1;
                m_vecPop[i].vecWeights.AddRange(weights[k]);
            }
		}
	}
	
	//	mutates a chromosome by perturbing its weights by an amount not 
	//	greater than MAX_PERTURBATION
	public void Mutate(List<double> chromo){
		//traverse the weight vector and mutate each weight dependent on the mutation rate
		for(int i = 0; i < chromo.Count; ++i){
			if(Random.value < m_dMutationRate)
				//add or substract a small value to the weight
				chromo[i] += ((double)Random.Range(-1.0f, 1.0f) * dMaxPerturbation);//dMaxPerturbation == 0.3
		}
	}
	
	// performs crossover at the neuron bounderies.
	public void CrossoverAtSplits(List<double> mum, List<double> dad, ref List<double> baby1, ref List<double> baby2){
		//return parents offspring dependent on the rate or
		//or if the parents are the same
		if((Random.value > m_dMutationRate) || (mum==dad)){
			baby1 = mum;
			baby2 = dad;
			
			return;
		}
		//determine a crossover point
		//int crsPoint = (int)Random.Range(0, m_iChromoLength - 1);
		int Index1 = (int)Random.Range(0, m_vecSplitPoints.Count-2);
  		int Index2 = (int)Random.Range(Index1, m_vecSplitPoints.Count-1);
		
		int cp1 = m_vecSplitPoints[Index1];
		int cp2 = m_vecSplitPoints[Index2];
		
		//create the offspring
		for (int i=0; i<mum.Count; ++i)
		{
			if ((i<cp1) || (i>=cp2)){
				baby1.Add(mum[i]);
				baby2.Add(dad[i]);
			}
			else{
				baby1.Add(dad[i]);
				baby2.Add(mum[i]);
			}
		}
		
		return;
	}
	
	//  performs standard tournament selection given a number of genomes to
	//  sample from each try.
	public SGenome TournamentSelection(int N){
		double BestFitnessSoFar = -999999;
		
		int chosenOne = 0;
		
	  	//Select N members from the population at random testing against 
  	    //the best found so far
		for(int i = 0; i < N; ++i){
			int ThisTry = (int)Random.Range(0, m_iPopSize-1);
			if (m_vecPop[ThisTry].dFitness > BestFitnessSoFar)
		    {

		      chosenOne = ThisTry;
		
		      BestFitnessSoFar = m_vecPop[ThisTry].dFitness;
		    }
		}
			
		//return the best fitness object
		return m_vecPop[chosenOne];
	}
	
	//	takes a population of chromosones and runs the algorithm through one
	//	cycle.
	//	Returns a new population of chromosones.
	public List<SGenome> Epoch(List<SGenome> old_pop){
		
		//assign the given population to the classes population
		m_vecPop = old_pop;
		
		//reset the appropriate variables
		Reset();
		
		//create a temp list to store new chromosones
		List<SGenome> vecNewPop = new List<SGenome>();
		
		CalculateBestWorstAvTot();
		 
		//sort the population(for scaling and elitism)
        m_vecPop = m_vecPop.OrderBy(p => p.dFitness).ToList();
		
		//now to add a little elitism we shall add in some copies of the 
		//fittest genomes. Make sure we add an EVEN  number 
		if((iNumCopiesElite * iNumElite % 2)==0)
			vecNewPop = GrabNBest(iNumElite, iNumCopiesElite);
		
		//Debug.Log("vecnewpop count before : " + vecNewPop.Count);
		
		//enter the GA Loop
		//repeat until a new population is generated
		//Debug.Log("popsize : "+ m_iPopSize);
		while (vecNewPop.Count < m_iPopSize){
			//select using tournament selection for a change
			SGenome mum = TournamentSelection(iTournamentCompetitors);
			SGenome dad = TournamentSelection(iTournamentCompetitors);
			
			//create some offspring via crossover
			List<double> baby1 = new List<double>();
			List<double> baby2 = new List<double>();
			
			CrossoverAtSplits(mum.vecWeights, dad.vecWeights, ref baby1, ref baby2);
			
			//Debug.Log("baby1 count : " + baby1.Count);
			
			//now we mutate
			Mutate(baby1);
			Mutate(baby2);
			
			/*for(int i =0; i <baby1.Count; i++)
				Debug.Log("baby1: "+baby1[i]);
			
			for(int i =0; i <baby2.Count; i++)
				Debug.Log("baby2:"+baby2[i]);*/
			
			//now copy into vecNewPop population
			vecNewPop.Add(new SGenome(baby1, 0));
			vecNewPop.Add(new SGenome(baby2, 0));
		}
		
		//finished so assign new pop back into m_vecPop
		m_vecPop = vecNewPop;
		
		//Debug.Log("vecpop count : " + m_vecPop.Count);
		return m_vecPop;
	}
	
	//	This works like an advanced form of elitism by inserting NumCopies
	//  copies of the NBest most fittest genomes into a population vector
	public List<SGenome> GrabNBest(int NBest, int NumCopies){
		List<SGenome> Pop = new List<SGenome>();

		//add the require amout of copies of the n most fittest to the supplied vector
		while(NBest != 0){
			for(int i=0; i < NumCopies; ++i){
				Pop.Add(m_vecPop[(m_iPopSize-1) - NBest]);
			}
			NBest--;
		}
		
		return Pop;
	}
	
	//	calculates the fittest and weakest genome and the average/total 
	//	fitness scores
	public void CalculateBestWorstAvTot(){
		m_dTotalFitness = 0;
		double HighestSoFar = 0;
		double LowestSoFar = 9999999;
		
		for(int i =0 ; i<m_iPopSize; ++i){
			//update fittest if nessesary
			if(m_vecPop[i].dFitness > HighestSoFar){
				HighestSoFar = m_vecPop[i].dFitness;
				m_iFittestGenome = i;
				m_dBestFitness = HighestSoFar;
			}
			//update worst if neccesary
			if(m_vecPop[i].dFitness < LowestSoFar){
				LowestSoFar = m_vecPop[i].dFitness;
				m_dWorstFitness = LowestSoFar;
			}
			
			m_dTotalFitness += m_vecPop[i].dFitness;
		}//next chromo
		
		m_dAverageFitness = m_dTotalFitness / m_iPopSize;
	}
	
	//	resets all the relevant variables ready for a new generation
	//--------------------------------------------------------------
	public void Reset(){
		m_dTotalFitness = 0;
		m_dBestFitness = 0;
		m_dWorstFitness = 9999999;
		m_dAverageFitness = 0;
	}
}
