using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkTrain : MonoBehaviour {
 	#region Member Variables
	//private NeuralNetworkTesting.NeuralNet net;
	private NeuralNet net;
    double high, mid, low;
	#endregion
	
	// Use this for initialization
	void Start () {
		Debug.Log("Starting network ");
		high = 0.99;
        low = 0.01;
        mid = 0.5;
	}
	
	// Update is called once per frame
	void Update () {
		#region Declarations
		double ll, lh, hl, hh;
		int count, iterations;
		bool verbose;            
		double[][] input, output;
		#endregion
		//StringBuilder bld;
		
		#region Initialization
		net = new NeuralNet();
        //bld = new StringBuilder();
		
        input = new double[4][];
        input[0] = new double[] { high, high };
        input[1] = new double[] { low, high };
        input[2] = new double[] { high, low };
        input[3] = new double[] { low, low };

        output = new double[4][];
        output[0] = new double[] { low };
        output[1] = new double[] { high };
        output[2] = new double[] { high };
        output[3] = new double[] { low };
		
        verbose = false;
        count = 0;
        iterations = 5;
		#endregion
		
		#region Execution
		// initialize with 
        //   2 perception neurons
        //   2 hidden layer neurons
        //   1 output neuron
		net.Initialize(1, 2, 2, 1);
		Debug.Log("2 perception neurons, 2 hidden layer neurons, 1 output neurons");
       	Debug.Log("Training");
		/*do
        {*/
		for(int i = 0; i < 1000 ; i ++){
			Debug.Log("Train count : " + count);
        	count++;

        	net.LearningRate = 3;
            net.Train(input, output, TrainingType.BackPropogation, iterations);

            net.PerceptionLayer[0].Output = low;
            net.PerceptionLayer[1].Output = low;
            net.Pulse();

            ll = net.OutputLayer[0].Output;

            net.PerceptionLayer[0].Output = high;
            net.PerceptionLayer[1].Output = low;

            net.Pulse();

            hl = net.OutputLayer[0].Output;

            net.PerceptionLayer[0].Output = low;
            net.PerceptionLayer[1].Output = high;

            net.Pulse();

            lh = net.OutputLayer[0].Output;

            net.PerceptionLayer[0].Output = high;
            net.PerceptionLayer[1].Output = high;

            net.Pulse();

            hh = net.OutputLayer[0].Output;
				
			#region verbose
            if (verbose)
            {
				Debug.Log("hh : " + hh);
				Debug.Log("ll : " + ll);
				Debug.Log("hl : " + hl);
				Debug.Log("lh : " + lh);
				
            }
		}
				#endregion
		/*}while (hh > (mid + low)/2 || lh < (mid + high)/2 || hl < (mid + low) /2|| ll > (mid + high)/2);*/
		
            net.PerceptionLayer[0].Output = low;
            net.PerceptionLayer[1].Output = low;

            net.Pulse();

            ll = net.OutputLayer[0].Output;

            net.PerceptionLayer[0].Output = high;
            net.PerceptionLayer[1].Output = low;

            net.Pulse();

            hl = net.OutputLayer[0].Output;

            net.PerceptionLayer[0].Output = low;
            net.PerceptionLayer[1].Output = high;

            net.Pulse();

            lh = net.OutputLayer[0].Output;

            net.PerceptionLayer[0].Output = high;
            net.PerceptionLayer[1].Output = high;

            net.Pulse();

            hh = net.OutputLayer[0].Output;
		
            Debug.Log((count * iterations) + " iterations required for training\n");

            Debug.Log("hh: " + hh + " < .5\n");
            Debug.Log("ll: " + ll + " < .5\n");

            Debug.Log("hl: " + hl + " > .5\n");
            Debug.Log("lh: " + lh + " > .5\n");
		#endregion
	}
}
