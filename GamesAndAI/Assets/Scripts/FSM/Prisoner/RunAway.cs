/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class executes the actions needed for the prisoner 
 * character in the RunAway state (win game) */
public class RunAway : FSMState<Prisoner> {
	
	static readonly RunAway instance = new RunAway();
	private movementCS _movement;
	private GameObject runAwayNode;
	
    /* Return the state instance */
	public static RunAway Instance {
		get {
			return instance;
		}
	}
	static RunAway() { }
	private RunAway() { }
	
    /* Execute entering actions */
	public override void Enter (Prisoner p) {
		runAwayNode = GameObject.Find("Run Away");
		_movement = p.GetComponent<movementCS>();
	}
	
    /* When close to the exit of the prison, make the prisoners 
     * move towards it and finish the game */
	public override void Execute (Prisoner p) {	
		/* you've nearly won the game - quick! */
		_movement.Seek(runAwayNode.transform.position);
	}
	
	public override void Exit(Prisoner p) {
		
	}
}
