/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class handles the states for the prisoner character */
public class Prisoner : MonoBehaviour {
	
	private FiniteStateMachine<Prisoner> FSM;
		
	// Use this for initialization
	void Start () {
		FSM = new FiniteStateMachine<Prisoner>();
		FSM.Configure(this, Follow.Instance);
	}
	
	// Update is called once per frame
	void Update () {
		FSM.Update();
	}
	
    /* Change the state when needed */
	public void ChangeState(FSMState<Prisoner> e) {
		FSM.ChangeState(e);		
	}
	
    /* Accesor to the FSM instance */
	public FiniteStateMachine<Prisoner> fsm
	{
		get{
			return FSM;
		}
	}
}
