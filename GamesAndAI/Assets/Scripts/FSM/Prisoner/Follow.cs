/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class executes the actions needed for the prisoner 
 * character in the Follow state (win game) */
public class Follow : FSMState<Prisoner> {
	
	static readonly Follow instance = new Follow();
	public GameObject leader;
	private GameObject runAwayNode;

    /* Return the state instance */
	public static Follow Instance {
		get {
			return instance;
		}
	}
	static Follow() { }
	private Follow() { }
	
		
	public override void Enter (Prisoner p) {
		runAwayNode = GameObject.Find("Run Away");

        p.gameObject.GetComponent<FlockController>().EnableChildren();
    }

    /* Track for human input to change the state 
     * also verify if the game is won */
	public override void Execute (Prisoner p) {		
		/* check for player input */
		
		if(Input.GetKeyUp(KeyCode.H))
		{
			p.ChangeState(Hide.Instance);
		}
		
		/* check if exit is close */
		if(Vector3.Distance(p.transform.position, runAwayNode.transform.position) < 10)
		{
			p.ChangeState(RunAway.Instance);
		}	
	}
	
	public override void Exit(Prisoner p) {
		
	}
}
