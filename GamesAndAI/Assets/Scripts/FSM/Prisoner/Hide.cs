/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/* This class executes the actions needed for the prisoner 
 * character in the Hide state (win game) */
public class Hide : FSMState<Prisoner> {
	
    static readonly Hide instance = new Hide();
	GameObject[] hideNodes;
    
    /* Return the state instance */
	public static Hide Instance
	{
		get{
			return instance;
		}
	}
	
	static Hide(){}
	public Hide(){}

    /* Execute entering actions
     * In this case, the command is sent to the prisoner's leader
     * and he will command the group of prisoners */
    public override void Enter(Prisoner entity)
    {
        Dictionary<List<PathNode>, int> paths = new Dictionary<List<PathNode>, int>();
        hideNodes = GameObject.FindGameObjectsWithTag("Hide Node");
        
        /* Get the path generator instance */
        var nodeGenerator = GameObject.Find("NodeGenerator").GetComponent<PathNodeTester>();

        /* store the distance to the hide nodes */
        hideNodes.ToList().ForEach(node =>
            {
                var path = nodeGenerator.GetNewPath(entity.transform.position, node.transform.position);
                paths.Add(path, path.Count);
            });
        
        /* sort the list to get the two closest */
        var ordered = (from n in paths orderby n.Value ascending select n);
                
        /* make them hide */
        entity.gameObject.GetComponent<FlockController>().DisableChildren(ordered.Take(2).ToList());
    }

    /* Track for human input to change the state */
    public override void Execute(Prisoner entity)
    {
        /* player wants flock to not hide anymore */
        if (Input.GetKeyUp(KeyCode.H))
        {
            entity.ChangeState(Follow.Instance);
        }
    }
	
	public override void Exit (Prisoner entity)
	{
		
	}
}