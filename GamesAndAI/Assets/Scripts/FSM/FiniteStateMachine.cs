/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/

/* This class provides the Finite State Machine implementation */
public class FiniteStateMachine<T>
{
	private T Owner;
	private FSMState<T> CurrentState;
	private FSMState<T> PreviousState;
	private FSMState<T> GlobalState;
	
    /* Initialize values */
	public void Awake()
	{		
		CurrentState = null;
		PreviousState = null;
		GlobalState = null;
	}
	
	/* set default configuration */
	public void Configure(T owner, FSMState<T> InitialState) {
		Owner = owner;
		ChangeState(InitialState);
	}

    /* Check for state changes */
	public void Update()
	{
		if (GlobalState != null)  GlobalState.Execute(Owner);
		if (CurrentState != null) CurrentState.Execute(Owner);
	}
 
	/* Change the current state - execute the previous state's Exit function and
	 * the new state's Enter function */
	public void  ChangeState(FSMState<T> NewState)
	{	
		PreviousState = CurrentState;
 
		if (CurrentState != null)
			CurrentState.Exit(Owner);
 
		CurrentState = NewState;
 
		if (CurrentState != null)
			CurrentState.Enter(Owner);
	}
 
	/* if needed */
	public void  RevertToPreviousState()
	{
		if (PreviousState != null)
			ChangeState(PreviousState);
	}
	
	/* get the current state - compare to the state Instance */
	public FSMState<T> GetCurrentState()
	{
		return CurrentState;
	}
}