/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class executes the actions needed for the police 
 * character in the patrol state */
public class Patrol : FSMState<Police> {
	
	static readonly Patrol instance = new Patrol();
	private RaycastHit outInfo;
	private GameObject leader;
	private GameController gameController;
	
    /* Return the state instance */
	public static Patrol Instance {
		get {
			return instance;
		}
	}

	static Patrol() { }
	private Patrol() { }
	
	/* Execute entering actions */
	public override void Enter (Police p) {
		p.GetComponent<Wandering>().enabled = true;
		leader = GameObject.Find("Leader");
		gameController = GameObject.Find("GameController").GetComponent<GameController>();
	}
	
    /* Constantly execute the actions needed when the police
     * character is in the Patrol state*/
	public override void Execute (Police p) {
		/* find prisoners within sight and chase them */
		if(Physics.Raycast(p.transform.position, p.transform.forward, out outInfo, 10.0f) ||
			Physics.Raycast(p.transform.position, (-p.transform.right + p.transform.forward), out outInfo, 5.0f) ||
			Physics.Raycast(p.transform.position, (p.transform.right + p.transform.forward), out outInfo, 5.0f))
		{
			if(!gameController.IsFinished)
			{
				/* don't chase prisoners who have won the game */
				if(outInfo.collider.gameObject.tag.Equals("Prisoner") && leader.GetComponent<Prisoner>().fsm.GetCurrentState()
					!= RunAway.Instance)
				{
					p.SetTarget(outInfo.collider.gameObject);
					p.ChangeState(Chase.Instance);
				}
			}
		}
	}
	
    /* Exit actions executed when the police changes state */
	public override void Exit(Police p) {
		p.GetComponent<Wandering>().enabled = false;
	}
}
