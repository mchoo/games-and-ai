/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* This class handles the states for the police character */
public class Police : MonoBehaviour {
	
    /* References needed for the FSM */
	private FiniteStateMachine<Police> FSM;
	private GameObject target;
	public string currentState;
	public CNeuralNet nnet;
    public double m_dFitness;
	
	// Use this for initialization
	void Start () {
		FSM = new FiniteStateMachine<Police>();
		FSM.Configure(this, Patrol.Instance);
	}

    public void initNN()
    {
        /*Input:3 feelers + 3 raycast distances
        1 output as Yrotation
        only one hidden layer with 6 neurons*/
        nnet = new CNeuralNet(6, 1, 1, 6);
    }

	// Update is called once per frame
	void Update () {
		FSM.Update();
		currentState = FSM.GetCurrentState().ToString();
	}

    public void testNN(List<double> inputs)
    {
        /*test NN*/
        var move = GetComponent<movementCS>();
        
        var output = nnet.Update(inputs);
        /* validate num of outputs */
        if (output.Count < 1)
        {
            return;
        }
        
        //Debug.Log("output : " + output[0]);
        
        /* make the police to rotate in Y */
        transform.Rotate(new Vector3(0, (float)output[0], 0));
    }
	
    /* Change to a desired state and update */
	public void ChangeState(FSMState<Police> e) {
		FSM.ChangeState(e);		
	}
	
    /* Accessor for the state machine */
	public FiniteStateMachine<Police> fsm
	{
		get{
			return FSM;
		}
	}
	
    /* Set Target to pursue when the state is changed to chase */
	public void SetTarget(GameObject go)
	{
		target = go;
	}

    /* Target to pursue when the state is changed to chase */
	public GameObject Target
	{
		get{
			return target;
		}
	}

    public void EndOfRunCalculations()
    {
        m_dFitness += GetComponent<Mapper>().NumCellsVisited();
    }

    public void Reset()
    {
        m_dFitness = 0;
    }
}
