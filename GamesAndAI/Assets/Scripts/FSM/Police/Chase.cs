/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class executes the actions needed for the police 
 * character in the chase state */
public class Chase : FSMState<Police> {

	static readonly Chase instance = new Chase();
	private movementCS _movement;
	private Transform leader;
	private float timeSinceSeen;
	private RaycastHit hitInfo;
	private GameObject[] allPrisoners;
	
    /* Return the state instance */
	public static Chase Instance {
		get {
			return instance;
		}
	}
	static Chase() { }
	private Chase() { }

    /* Execute entering actions */
	public override void Enter (Police p) {
		timeSinceSeen = Time.timeSinceLevelLoad;
		_movement = p.GetComponent<movementCS>();
		allPrisoners = GameObject.FindGameObjectsWithTag("Prisoner");
	}

    /* Constantly execute the actions needed when the police
     * character is in the Chase state*/
	public override void Execute (Police p) {	
		
		/* setting time when first see prisoner */
		if(Physics.Raycast(p.transform.position, p.transform.forward * 10.0f, out hitInfo, 10.0f))
		{
			if(hitInfo.collider.tag == "Prisoner")
			{
				timeSinceSeen = Time.timeSinceLevelLoad;
			}
		}
		
		/* set police back to patrol if haven't seen prisoner in a while */
		if(Time.timeSinceLevelLoad - timeSinceSeen > 6)
		{
			p.ChangeState(Patrol.Instance);
			return;
		}

		_movement.Persue(p.Target);
		
		/* near enough to prisoner to capture */
		if(Vector3.Distance(p.Target.transform.position, p.gameObject.transform.position) < 4.5f)
		{
			p.ChangeState(Capture.Instance);
		}
		
		/* set target to closest prisoner */
		float closest = Vector3.Distance(allPrisoners[0].transform.position, p.transform.position);
		float currentDist;
		foreach(GameObject go in allPrisoners)
		{
			currentDist = Vector3.Distance(go.transform.position, p.transform.position);
			if(currentDist < closest)
			{
				closest = currentDist;
				p.SetTarget(go);
			}
		}
				
	}
	
	public override void Exit(Police p) {
		
	}
}
