/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
using UnityEngine;
using System.Collections;

/* This class executes the actions needed for the police 
 * character in the Capture state */
public class Capture : FSMState<Police> {
	private float cdTime;
	public float minInterval = 0.3f;
	static readonly Capture instance = new Capture();
	
	/* Return the state instance */
	public static Capture Instance {
		get {
			return instance;
		}
	}
	
	static Capture() { }
	private Capture() { }

    /* Execute entering actions: Capture the prisoner and go back to patrol */
	public override void Enter (Police p) {
		cdTime = Time.timeSinceLevelLoad;
		
	}
	
	public override void Execute (Police p) {		
		if(Time.timeSinceLevelLoad  - cdTime > minInterval)
			p.ChangeState(Patrol.Instance); // go back to patrolling
	}
	
	public override void Exit(Police p) {
		/* kill target */
		p.Target.active = false;
	}
}
