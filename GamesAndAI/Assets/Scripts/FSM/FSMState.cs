/****************************************************************
* COSC2527/2528 Games and Artificial Intelligence Techniques
* Semester 1 2013 Assignment #2
* Student Numbers  : 3333466, 3323588, 3343653
*****************************************************************/
/* This class provides the generic definition for a FSM State */
abstract public class FSMState<T>
{

	abstract public void Enter (T entity);
		
	abstract public void Execute (T entity);

	abstract public void Exit(T entity);
}
