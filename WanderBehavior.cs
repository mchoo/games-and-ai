using UnityEngine;
using System.Collections;
sdfsdfds
public class WanderBehavior : MonoBehaviour
{
    private enum WanderingState { Moving = 0, Pausing = 1 }

    public float wanderRadius = 8.0f;
    public float wanderPauseTimeMin = 2.0f;
    public float wanderPauseTimeMax = 6.0f;
    public float speed = 2.0f;
    public AnimationClip wanderWalkAnimation;
    public AnimationClip[] wanderIdleAnimationList;

    private Vector3 basePosition;
    private Vector3 currentDestination;
    private WanderingState currentState;
    private float pauseTimeout;

    void Start()
    {
        basePosition = transform.position;
        ChooseNextDestination();
        currentState = WanderingState.Moving;
        animation.Play(wanderWalkAnimation.name);
    }

    void ChooseNextDestination()
    {
        Vector2 randOffset = Random.insideUnitCircle * wanderRadius;
        currentDestination = basePosition + new Vector3(randOffset.x, transform.position.y, randOffset.y);
    }

    void PlayRandomIdleAnimation()
    {
        animation.CrossFade(wanderIdleAnimationList[Random.Range(0, wanderIdleAnimationList.GetLength(0))].name, 0.2f);
    }

    void Update()
    {
        Debug.DrawLine(transform.position, currentDestination, Color.white);

        if (currentState == WanderingState.Pausing)
        {
            pauseTimeout -= Time.deltaTime;
            if (pauseTimeout <= 0.0f)
            {
                ChooseNextDestination();
                currentState = WanderingState.Moving;
                animation.CrossFade(wanderWalkAnimation.name, 0.2f);
            }
            return;
        }
        else if (currentState == WanderingState.Moving)
        {
            currentDestination = new Vector3(currentDestination.x, transform.position.y, currentDestination.z);
            transform.forward = currentDestination - transform.position;
            transform.Translate(Vector3.forward * speed * Time.deltaTime);

            if ((currentDestination - transform.position).magnitude < 0.1f)
            {
                pauseTimeout = Random.Range(wanderPauseTimeMin, wanderPauseTimeMax);
                currentState = WanderingState.Pausing;
                PlayRandomIdleAnimation();                
            }
        }
    }

    void LateUpdate()
    {
        transform.position = new Vector3(transform.position.x, Terrain.activeTerrain.SampleHeight(transform.position), transform.position.z);
    }
}